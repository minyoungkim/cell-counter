##--------------------------------
# main.py
#
# created on 08/15/2017
#
# - Entry Point for the program
#---------------------------------

#----------
# imports
#----------
import sys
import os
import numpy as np

import util as pUtil
import data as pData
import parser as pParser


##---------
# main
#----------
if __name__ == "__main__":
    pprt = pUtil.PRT()
    pprt.p("[ Starting proj-CC... ]", pprt.STATUS)
    np.set_printoptions(threshold=np.nan)

    #--------------------
    # Parser
    #--------------------
    p = pParser.Parser()
    p.parse(sys.argv, "CC-Parser")


    #--------------------
    # Data Processing
    #--------------------
    dp = pData.DataProcessor(p)
    pprt.p("[ Data processing... ]", pprt.STATUS)
    dp.process()
    dp.wrapup()


    pprt.p("[ Finishing proj-CC... ]", pprt.STATUS)
