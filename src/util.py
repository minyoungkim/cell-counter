##--------------------------------
# util.py
#
# created on 08/15/2017
#
# - basic utils
#---------------------------------

#----------
# imports
#----------
import os
from os import system
from os.path import *
import inspect
import numpy as np
from termcolor import colored
from random import shuffle
import matplotlib.pyplot as plt
import scipy.stats as stats


##----------------------------------------------------------
# CLASS PRT()
# - print message to terminal with colors different by level
#-----------------------------------------------------------
class PRT(object):
    STATUS = "status-progress"
    ERROR = "error"
    WARNING = "warning"
    LOG = "log"
    COLOR = {
             STATUS:'magenta',
             ERROR:'red',
             WARNING:'yellow',
             LOG:'green'
            }


    def __init__(self):
        pass


    def p(self, _msg, _flag):
        if _flag not in self.COLOR.keys():
            print(colored("INVALID FLAG for PRT_MSG()!", 'red'))
            exit(1)

        print(colored(_msg, self.COLOR[_flag]))



##----------------------------------------------------------
# DEF WAIT
# - stop for input
#-----------------------------------------------------------
def WAIT(_str=""):
    st = inspect.stack()
    try:
        wait = input("WAIT(%s) %s"%(str(':'.join(np.array(st[1][1:4]))), _str))
    except:
        pass



##----------------------------------------------------------
# DEF CHECK_DIR
# - check if path exists, and create if not
#-----------------------------------------------------------
def CHECK_DIR(path, _check=True):
    if _check:
        if not isdir(path): 
            print("%s doesn't exist, thus create one..."%path)
            system("mkdir -p " + path)



#-----------------------------------------------------------
# DEF PLOT_HIST
# - plot histograms with provided data
#-----------------------------------------------------------
def PLOT_HIST(_data, _titles, _visf=None):
    plt.ion()
    fig = plt.figure(figsize=(20, 30))
    fig.suptitle("Histogram of Data", fontsize=16, weight='bold')

    dim = len(_data)

    for i in range(dim):
        subdata = _data[i]
        subdata.sort()
        max_val, min_val = np.max(subdata), np.min(subdata)
        mean = np.mean(subdata)
        std = np.std(subdata)
        pdf = stats.norm.pdf(subdata, mean, std)

        # plot
        ax = plt.subplot(dim, 1, i+1)
        title = _titles[i] + " histogram (mean: %f, std: %f, max: %f, min: %f)"%(mean, std, max_val, min_val)
        ax.set_title(title, fontsize=10, weight='bold')
        plt.plot(subdata, pdf, '-o', color='red')
        plt.hist(subdata, normed=True, color='yellow', alpha=0.6)

    plt.show()
    plt.ioff()

    if _visf:
        plt.savefig(_visf, dpi=5)
        print("plotHistograms(): histogram plot is saved in %s!"%_visf)

    return


