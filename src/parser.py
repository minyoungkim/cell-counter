##--------------------------------
# parser.py
#
# created on 08/15/2017
#
# - commandline parser
#---------------------------------

#----------
# imports
#----------
import argparse
import sys
import util as pUtil


##--------------------------------------------
# CLASS Parser
# - parse arguments
# --------------------------------------------
class Parser(object):
    prt = pUtil.PRT()

    def __init__(self):
        pass

    ##--------------------------------------------
    # DEF create_parser
    # - create argparse and return
    # --------------------------------------------
    def create_parser(self, desc=None):
        parser = argparse.ArgumentParser(description=desc)

        parser.add_argument('-dd', '--data_dir', help='data directory', default='./data')
        parser.add_argument('-dp', '--patch_save_dir', help='directory for patch to be saved', \
                                   default='./data/patches')
        parser.add_argument('-ng', '--num_gen', help='# of data to generate', default=5000)
        parser.add_argument('-mp', '--max_per_sample', help='max # of patches per sampling', default=300)
        parser.add_argument('-iw', '--img_w', type=int, default=2065)
        parser.add_argument('-ih', '--img_h', type=int, default=3600)
        parser.add_argument('-piw', '--patch_w', type=int, default=32)
        parser.add_argument('-pih', '--patch_h', type=int, default=32)
        parser.add_argument('-ithr', '--intensity_thr', type=float, default=0.15)
        parser.add_argument('-ts', '--time', help='timestamp that would automatically set', required=True)
        parser.add_argument('-sq', '--sequence_len', type=int, default=10, \
                                   help='sequence len [1 for non-sequence learning]')
        parser.add_argument('-dbg', '--debug', action='store_true', help='debug mode [on/off]')

        return parser



    ##--------------------------------------------
    # DEF parse
    # - parse arguments and set parameters
    # --------------------------------------------
    def parse(self, argv, desc):
        parser = self.create_parser(desc)
        options = parser.parse_args(argv[1:])

        self.data_dir = options.data_dir
        self.intensity_thr = float(options.intensity_thr)     # 0.09, 0.13, 0.25, 0.3

        # prep required directories
        self.patch_save_dir_prefix = '/'.join([options.patch_save_dir, "thr_%.2f"%self.intensity_thr])
        self.patch_tif_dir = '/'.join([self.patch_save_dir_prefix, "tiff16"])
        pUtil.CHECK_DIR(self.patch_tif_dir)
        self.patch_gif_dir = '/'.join([self.patch_save_dir_prefix, "gif8"])
        pUtil.CHECK_DIR(self.patch_gif_dir)
        self.patch_png_dir = '/'.join([self.patch_save_dir_prefix, "png16"])
        pUtil.CHECK_DIR(self.patch_png_dir)


        self.num_gen = int(options.num_gen)
        self.max_per_sample = int(options.max_per_sample)
        self.imgw = int(options.img_w)
        self.imgh = int(options.img_h)
        self.pw = int(options.patch_w)
        self.ph = int(options.patch_h)
        self.timestmp = options.time
        self.seqlen = options.sequence_len
        self.debug = options.debug

        self.unique_id = self.timestmp[0:8] + self.timestmp[9:]

        # print all params if in debug mode
        if self.debug:
            self.print_params()



    ##--------------------------------------------
    # DEF print_params
    # - print all params set
    # --------------------------------------------
    def print_params(self):
        attrs = vars(self)
        entry = '\n'.join("[ %s ]\t: %s" % item for item in sorted(attrs.items()))

        title = "[------------------ %s() Variables and their values ------------------]"%self.__class__.__name__
        self.prt.p(title, self.prt.LOG)
        self.prt.p("%s"%entry, self.prt.LOG)
        self.prt.p(title, self.prt.LOG)



