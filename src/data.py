##--------------------------------
# data.py
#
# created on 08/15/2017
#
# - data processing scripts
#---------------------------------

#----------
# imports
#----------
import glob
import cv2
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
from random import shuffle, randint
import imageio
import numpngw as npngw
from progressbar import ProgressBar, Bar, Percentage, FormatLabel, RotatingMarker
from sklearn.cluster import AffinityPropagation
from sklearn import metrics

import util as pUtil



#---------------------------------------------
# some thoughts
# - I try counting a small random region
#   which is roughly 193x216(= 41688 pixels),
#   and found around 30 cells (maybe max)
# - In an img with resolution 2065(w)x3600(h),
#   there're roughly 178 windows with 
#   size of the region as a window
# - and there can be 5350 cells
# - however, most of the images have NO
#   information on 1/3 portion,
# - also the sparcity on certain areas
#   let's say 3/5
# - which ended up having 2140 cells?
#
# - Thus,
#   when setting up intensity threshold,
#   assuming the middle point (in z-axis) has
#   the strongest intensity,
#   I would say to set it as the # of POI in
#   a random image is around 500.
#   
#---------------------------------------------



##--------------------------------------------
# CLASS DataProcessor
# - data process class
# --------------------------------------------
class DataProcessor(object):
    prt = pUtil.PRT()
    
    # constructor
    def __init__(self, _p):
        self.p = _p

        self.normalize = False
        self.gifs = []
        self.dcnt = 0
        self.max_v_idices = []

        self.loadFiles()



    ##------------------------
    # DEF loadFiles()
    #-------------------------
    def loadFiles(self):

        self.files = sorted(glob.glob('/'.join([self.p.data_dir, '*.tif'])))
        self.nf = len(self.files)

#        self.gmax = self.calculateMax()    # TODO: but outside of the frame as it takes much memory
        self.gmax = 32026
        if self.normalize:
            self.gmax = float(self.gmax) / 65535.



    ##------------------------
    # DEF process()
    # - main api for the class
    #-------------------------
    def process(self):
        # resolution: [ 751, 3600, 2065 ] ( d x h x w )
        self.prt.p("Total %d files are retrieved from %s"%(self.nf, self.p.data_dir), self.prt.LOG)
        pUtil.WAIT("proceed? [Press Enter]")

        processed = []

        widgets= [FormatLabel('Total(%d/%d)'%(self.dcnt, self.p.num_gen)), 
                  Bar('=', '[', ']'), ' ', Percentage(), '\n']
        bar = ProgressBar(widgets=widgets, maxval=self.p.num_gen)
        bar.start()
        epoch = 1
        while self.dcnt < self.p.num_gen:
            picked = randint(0, self.nf-self.p.seqlen-1)
            if picked in processed:
                self.prt.p("%d already processed once (skip)"%picked, self.prt.WARNING)
                continue
            processed.append(picked)

            # load images and generate patches
            images, ffn = self.loadImages(picked)
            total = self.generatePatches(images, ffn, epoch)
            if total:
                epoch += 1
                widgets[0] = FormatLabel('Total(%d/%d)'%(self.dcnt, self.p.num_gen))
                bar.update(self.dcnt)
        bar.finish()



    ##------------------------
    # DEF generatePatches()
    #-------------------------
    def generatePatches(self, _images, _ffn, _step):
        locs = self.findROI(_images, self.p.max_per_sample)
        if locs is None:
            return 
        if self.p.debug:
            self.prt.p("found %d ROIs"%len(locs), self.prt.LOG)

        cnt = 0
        for idx in tqdm(range(len(locs)), desc="-----(Round %d with %s))"%(_step, _ffn)):
            loc = locs[idx]
            if self.dcnt == self.p.num_gen:
                return cnt
            self.generateGIF(_images, loc, _ffn)
            cnt += 1

        return len(locs)
            


    ##------------------------
    # DEF generateGIF()
    #-------------------------
    def generateGIF(self, _images, _loc, _ffn):
        cy, cx = _loc
        x = max(cx - int(self.p.pw/2.), 0)
        y = max(cy - int(self.p.ph/2.), 0)
        patches = []
        try:
            for i, img in enumerate(_images):
                patch = img[y:y+self.p.ph, x:x+self.p.pw]
                assert patch.shape == (self.p.ph, self.p.pw)
                patches.append(patch)
                if self.p.debug:
                    cv2.imshow('16bit TIFF_%d'%i, patch)
        except:
            self.prt.p("Exception occured during cropping!", self.prt.ERROR)
            return 
            
        if self.p.debug:
            cv2.waitKey()
            cv2.destroyAllWindows()
	
        # save .tif
        base = "patch_%d_%s_%d_%d"%(self.dcnt, _ffn, x, y)
        tifbase = '/'.join([self.p.patch_tif_dir, base])
        for idx, p in enumerate(patches):
            tifn = tifbase + "_%02d.tif"%idx
            cv2.imwrite(tifn, p)

        # save gif and png (8bit and 16bit)
        gifname = '/'.join([self.p.patch_gif_dir, base +"_8bit.gif"])
        pngname = '/'.join([self.p.patch_png_dir, base +"_16bit.png"])
        imageio.mimsave(gifname, patches, duration=0.2)
        npngw.write_apng(pngname, patches, delay=250, use_palette=True)

        # update global counter
        self.dcnt += 1

        

    def findROI(self, _img, _max=0):
        locs = []
        max_volume = 0
        max_v_idx = 0
        best_coords = 0

        for idx, img in enumerate(_img):
            y, x = np.where(img > self.p.intensity_thr*self.gmax)
            if len(x) == 0:
                if self.p.debug:
                    self.prt.p("couldn't find and ROI with img idx %d"%idx, self.prt.LOG)
                    cv2.imshow('NO ROI', img)
                    cv2.waitKey()

                return None

            coords = list(zip(y, x))
            if len(coords) > max_volume:
                max_volume = len(coords)
                max_v_idx = idx
                best_coords = coords

        if self.p.debug and not self.normalize:
            cv2.imshow('Selected step', _img[max_v_idx])
            cv2.waitKey()

        # do affinity prop 
        af = AffinityPropagation(preference=-60).fit(best_coords)
        center_indices = af.cluster_centers_indices_
        labels = af.labels_
        self.prt.p("clustered to %d centers from Total %d ROI points"%(len(center_indices), max_volume), \
                   self.prt.STATUS)
	self.prt.p("Silhouette Coefficient: %0.3f"%metrics.silhouette_score(best_coords, \
						   labels, metric='sqeuclidean'), self.prt.STATUS)

	# wrap up
        self.max_v_idices.append(max_v_idx)
	clustered_best_coords = np.array(best_coords)[center_indices]


        if _max:
            shuffle(clustered_best_coords)
            return clustered_best_coords[:_max]

        return clustered_best_coords



    ##------------------------
    # DEF loadImages()
    #-------------------------
    def loadImages(self, _idx):
        images = []
        first_filename = None
        for i in range(_idx, _idx+self.p.seqlen):
            f = self.files[i]
            if not first_filename:
                first_filename = f.split('/')[-1].split('.')[0]

            img = cv2.imread(f, -1)
            if self.normalize:
                img_n = img.astype(float) / 65535.
                images.append(img_n)
            else:
                images.append(img)

        return images, first_filename



    ##------------------------
    # DEF wrapup()
    #-------------------------
    def wrapup(self):

        self.prt.p("AVG max_v_idx: %.2f"%np.mean(self.max_v_idices), self.prt.LOG)
        self.prt.p("MEDIAN max_v_idx: %.2f"%np.median(self.max_v_idices), self.prt.LOG)




