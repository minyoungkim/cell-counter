#!/bin/sh


palette="patch_0_184855_171133_033350_1058_95_00.tif"
filters="fps=15,scale=320:-1:flags=lanczos"

#ffmpeg -v warning -i $1 -vf "$filters,palettegen" -y $palette
#ffmpeg -v warning -i $1 -i $palette -lavfi "$filters [x]; [x][1:v] paletteuse" -y $2
ffmpeg -i patch_0_184855_171133_033350_1058_95_%02d.tif -vf palettegen palette.png
ffmpeg -v warning -i patch_0_184855_171133_033350_1058_95_%02d.tif -i palette.png  -lavfi "paletteuse,setpts=6*PTS" -y out_palette.gif
