## PROJECT-CC: Cell-Counter
counting cells using machine learning algorithm

> Developed by: **Minyoung Kim** (mykim@alumni.stanford.edu)

----------


> **[ Usage ]**
> 
> run 'make' for usage
> ```
> make
> // ============== Project-CC: Cell-Counter Usage ==============
> //  1. install requirements
> //      make setup.x
> //  2. process data
> //      make data.x
> //      make data.x.debug (for debug mode)
> ============== ------------------------------ ==============
> ```

----------

## Requirements

Install required packages:
```
make setup.x
```


> **Python 3.5**
> tested on 3.5.2


----------

## Data Processing
```
make data.x
```

----------

