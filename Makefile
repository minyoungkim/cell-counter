#-----------------------
# Makefile
# 
# created on 08/15/2017
#-----------------------

TIMESTMP := $(shell /bin/date "+%Y%m%d-%H%M%S")


all:
	$(MAKE) usage.x

usage.x:
	bash shell/usage.sh

setup.x:
	bash shell/setup.sh

data.x:
	python src/main.py -ts $(TIMESTMP) -dd ./data/raw -ng 5000 -ithr 0.11

data.x.debug:
	python src/main.py -ts $(TIMESTMP) -dd ./data/raw -ng 50 --debug

clean.x:
	rm `find . -name *.pyc`

